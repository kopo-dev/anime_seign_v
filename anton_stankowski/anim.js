var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
camera.position.z = 200;
//camera.position.x = -200;

let helper = new THREE.CameraHelper(camera);
scene.add(helper);

//var geometry = new THREE.BoxGeometry( 1, 1, 1 );
//var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
//var cube = new THREE.Mesh( geometry, material );
//scene.add( cube );

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

//test des loaders, infructueux 
var manager = new THREE.LoadingManager();
var MTLLoader = new THREE.MTLLoader();
var OBJloader = new THREE.OBJLoader();
//var OBJloader2 = new THREE.OBJLoader2();

var controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.enableDamping = true;
controls.dampingFactor = 0.25;
controls.enableZoom = true;

var keyLight = new THREE.DirectionalLight(new THREE.Color('hsl(30, 100%, 75%)'), 1.0);
keyLight.position.set(-1000, 0, 1000);

var fillLight = new THREE.DirectionalLight(new THREE.Color('hsl(240, 100%, 75%)'), 0.75);
fillLight.position.set(1000, 0, 1000);

var backLight = new THREE.DirectionalLight(0xffffff, 1.0);
backLight.position.set(1000, 0, -1000).normalize();

var ambientLight = new THREE.AmbientLight(0xffffff);
scene.add(ambientLight);

scene.add(keyLight);
scene.add(fillLight);
scene.add(backLight);


var mesh = null;

MTLLoader.setPath("./models/")
MTLLoader.load(
    "forme.mtl",
    function(material) {
        console.log("Material has loaded");
        material.preload();
        OBJloader.setMaterials(material);
        OBJloader.setPath("./models/")
        OBJloader.load(
            "forme.obj",
            function(obj) {
                scene.add(obj);
                console.log(obj);
                obj.scale.set(0.1, 0.1, 0.1);
                obj.position.y += 50;
                obj.position.x -= 100;
                
                 
            },
            function ( xhr ) {
            console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );
            },
            function(err) {
                console.error("Error: ", err);
            }
        )
    },
    function ( xhr ) {
        console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );
    },
    function(err) {
            console.error("Error: ", err);
    }
)

function animate() {
    requestAnimationFrame( animate );
  //  cube.rotation.x += 0.01;
  //  cube.rotation.y += 0.01;
    renderer.render( scene, camera );
}
animate();