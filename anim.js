var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
camera.position.z = 20;
//camera.position.x = -200;

var chaos = false;
var rotate = false;
var translate = false;

//var geometry = new THREE.BoxGeometry( 1, 1, 1 );
//var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
//var cube = new THREE.Mesh( geometry, material );
//scene.add( cube );

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

var controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.enableDamping = true;
controls.dampingFactor = 0.25;
controls.enableZoom = true;
 
var keyLight = new THREE.DirectionalLight(new THREE.Color('hsl(30, 100%, 75%)'), 1.0);
keyLight.position.set(-100, 0, 100);

var fillLight = new THREE.DirectionalLight(new THREE.Color('hsl(240, 100%, 75%)'), 0.75);
fillLight.position.set(100, 0, 100);

var backLight = new THREE.DirectionalLight(0xffffff, 1.0);
backLight.position.set(100, 0, -100).normalize();

var ambientLight = new THREE.AmbientLight(0xffffff);
scene.add(ambientLight);

scene.add(keyLight);
scene.add(fillLight);
scene.add(backLight);

//ICI FAIRE DES CUBES ET LES SUPERPOSER
//gros cube de fond
var geometry = new THREE.BoxGeometry( 80, 38, 20 );
var material = new THREE.MeshBasicMaterial( { color: 0x2e8b57} );
var fond = new THREE.Mesh( geometry, material );
scene.add( fond );
fond.position.z -= 20

var geometry = new THREE.BoxGeometry( 17, 10, 5 );
var material = new THREE.MeshBasicMaterial( { color: 0xee9c00} );
var cudeux = new THREE.Mesh( geometry, material );
scene.add( cudeux );
cudeux.position.z -= 10
cudeux.position.y += 12
cudeux.position.x -= 25

var geometry = new THREE.BoxGeometry( 5, 5, 1 );
var material = new THREE.MeshBasicMaterial( { color: 0xc90000} );
var cutrois = new THREE.Mesh( geometry, material );
scene.add( cutrois );
cutrois.position.z -= 4
cutrois.position.y += 11
cutrois.position.x -= 25

var geometry = new THREE.BoxGeometry( 5, 5, 1 );
var material = new THREE.MeshBasicMaterial( { color: 0xc90000} );
var cuquat = new THREE.Mesh( geometry, material );
scene.add( cuquat );
cuquat.position.z -= 4
cuquat.position.y += 11
cuquat.position.x -= 18

var geometry = new THREE.BoxGeometry( 17, 22, 5 );
var material = new THREE.MeshBasicMaterial( { color: 0xcb63729} );
var cinq = new THREE.Mesh( geometry, material );
scene.add( cinq );
cinq.position.z -= 10
cinq.position.y -= 5
cinq.position.x -= 25

var geometry = new THREE.BoxGeometry( 8, 8, 1 );
var material = new THREE.MeshBasicMaterial( { color: 0xc000000} );
var six = new THREE.Mesh( geometry, material );
scene.add( six );
six.position.z -= 4
six.position.y -= 0
six.position.x -= 21

var geometry = new THREE.BoxGeometry( 8, 8, 1 );
var material = new THREE.MeshBasicMaterial( { color: 0xcFFA500} );
var sept = new THREE.Mesh( geometry, material );
scene.add( sept );
sept.position.z -= 4
sept.position.y -= 9
sept.position.x -= 21

var geometry = new THREE.BoxGeometry( 2, 2, 1 );
var material = new THREE.MeshBasicMaterial( { color: 0xc90000} );
var huit = new THREE.Mesh( geometry, material );
scene.add( huit );
huit.position.z -= 1
huit.position.y -= 8
huit.position.x -= 19

var geometry = new THREE.BoxGeometry(8, 8, 5 );
var material = new THREE.MeshBasicMaterial( { color: 0xcb63729} );
var neuf = new THREE.Mesh( geometry, material );
scene.add( neuf );
neuf.position.z -= 4.5
neuf.position.y += 10
neuf.position.x -= 5


var geometry = new THREE.BoxGeometry(3, 3, 1.5 );
var material = new THREE.MeshBasicMaterial( { color: 0xcFFA500} );
var dix = new THREE.Mesh( geometry, material );
scene.add( dix );
dix.position.z -= 1
dix.position.y += 9
dix.position.x -= 5

var geometry = new THREE.BoxGeometry(8, 8, 5 );
var material = new THREE.MeshBasicMaterial( { color: 0xcb63729} );
var onze = new THREE.Mesh( geometry, material );
scene.add( onze );
onze.position.z -= 4.5
onze.position.y += 10
onze.position.x += 4

var geometry = new THREE.BoxGeometry(3, 3, 1.5 );
var material = new THREE.MeshBasicMaterial( { color: 0xcFFA500} );
var douze = new THREE.Mesh( geometry, material );
scene.add( douze );
douze.position.z -= 1
douze.position.y += 9
douze.position.x += 4

var geometry = new THREE.BoxGeometry(18, 19, 5 );
var material = new THREE.MeshBasicMaterial( { color: 0xbf851c} );
var treize = new THREE.Mesh( geometry, material );
scene.add( treize );
treize.position.z -= 5
treize.position.y -= 5
treize.position.x -= 1

var geometry = new THREE.BoxGeometry(12, 12, 3 );
var material = new THREE.MeshBasicMaterial( { color: 0x982013} );
var quatorze = new THREE.Mesh( geometry, material );
scene.add( quatorze );
quatorze.position.z -= 2
quatorze.position.y -= 3
quatorze.position.x -= 1

var geometry = new THREE.BoxGeometry(8, 8, 3 );
var material = new THREE.MeshBasicMaterial( { color: 0x000000} );
var quinze = new THREE.Mesh( geometry, material );
scene.add( quinze );
quinze.position.z -= 1;
quinze.position.y -= 3;
quinze.position.x -= 1;

var geometry = new THREE.BoxGeometry(2.5, 2.5, 1 );
var material = new THREE.MeshBasicMaterial( { color: 0xff0000} );
var seize = new THREE.Mesh( geometry, material );
scene.add( seize );
seize.position.z -= 0;
seize.position.y -= 11;
seize.position.x -= 5.5;

var geometry = new THREE.BoxGeometry(2.5, 2.5, 1 );
var material = new THREE.MeshBasicMaterial( { color: 0x982013} );
var dixsept = new THREE.Mesh( geometry, material );
scene.add(dixsept);
dixsept.position.z -= 0
dixsept.position.y -= 11
dixsept.position.x -= 1

var geometry = new THREE.BoxGeometry(2.5, 2.5, 1 );
var material = new THREE.MeshBasicMaterial( { color: 0xff0000} );
var dixhuit = new THREE.Mesh( geometry, material );
scene.add( dixhuit );
dixhuit.position.z -= 0
dixhuit.position.y -= 11
dixhuit.position.x += 3


var geometry = new THREE.BoxGeometry(12, 16, 5 );
var material = new THREE.MeshBasicMaterial( { color: 0x981d0f} );
var dixneuf = new THREE.Mesh( geometry, material );
scene.add( dixneuf );
dixneuf.position.z -= 3
dixneuf.position.y += 5
dixneuf.position.x += 18


var geometry = new THREE.BoxGeometry(4, 4, 2 );
var material = new THREE.MeshBasicMaterial( { color: 0x742350} );
var vingt = new THREE.Mesh( geometry, material );
scene.add( vingt );
vingt.position.z -= 0
vingt.position.y += 10
vingt.position.x += 17


var geometry = new THREE.BoxGeometry(4, 4, 2 );
var material = new THREE.MeshBasicMaterial( { color: 0x000000} );
var vinun = new THREE.Mesh( geometry, material );
scene.add( vinun );
vinun.position.z -= 0
vinun.position.y += 5
vinun.position.x += 17


var geometry = new THREE.BoxGeometry(4, 4, 2 );
var material = new THREE.MeshBasicMaterial( { color: 0xFFA500} );
var deuxdeux = new THREE.Mesh( geometry, material );
scene.add( deuxdeux );
deuxdeux.position.z -= 0
deuxdeux.position.x += 17

var geometry = new THREE.BoxGeometry(9, 9, 2 );
var material = new THREE.MeshBasicMaterial( { color: 0xcFFA500} );
var vintrois = new THREE.Mesh( geometry, material );
scene.add( vintrois );
vintrois.position.z -= 0
vintrois.position.y -= 8.3
vintrois.position.x += 19


var geometry = new THREE.BoxGeometry(4, 4, 2 );
var material = new THREE.MeshBasicMaterial( { color: 0x982013} );
var vincat = new THREE.Mesh( geometry, material );
scene.add( vincat );
vincat.position.z += 1
vincat.position.y -= 8
vincat.position.x += 18

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function trueReset() {
  location.reload();
}

function reset() {
  chaos = false;
  rotate = false;
  translate = false;

  camera.position.z = 20;
  camera.position.y = 0;
  camera.position.x = 0;
  

  vincat.rotation.z = 0
vincat.rotation.y = 0
vincat.rotation.x = 0

vintrois.rotation.z = 0
vintrois.rotation.y = 0
vintrois.rotation.x = 0

deuxdeux.rotation.y = 0
deuxdeux.rotation.z = 0
deuxdeux.rotation.x = 0

vinun.rotation.z = 0
vinun.rotation.y = 0
vinun.rotation.x = 0

vingt.rotation.z = 0
vingt.rotation.y = 0
vingt.rotation.x = 0

dixneuf.rotation.z = 0
dixneuf.rotation.y = 0
dixneuf.rotation.x = 0

dixhuit.rotation.z = 0
dixhuit.rotation.y = 0
dixhuit.rotation.x = 0

dixsept.rotation.z = 0
dixsept.rotation.y = 0
dixsept.rotation.x = 0

seize.rotation.z = 0;
seize.rotation.y = 0;
seize.rotation.x = 0;

quinze.rotation.z = 0;
quinze.rotation.y = 0;
quinze.rotation.x = 0;

quatorze.rotation.z = 0
quatorze.rotation.y = 0
quatorze.rotation.x = 0

treize.rotation.z = 0
treize.rotation.y = 0
treize.rotation.x = 0

douze.rotation.z = 0
douze.rotation.y = 0
douze.rotation.x = 0

onze.rotation.z = 0
onze.rotation.y = 0
onze.rotation.x = 0

dix.rotation.z = 0
dix.rotation.y = 0
dix.rotation.x = 0

neuf.rotation.z = 0
neuf.rotation.y = 0
neuf.rotation.x = 0

huit.rotation.z = 0
huit.rotation.y = 0
huit.rotation.x = 0

sept.rotation.z = 0
sept.rotation.y = 0
sept.rotation.x = 0

six.rotation.z = 0
six.rotation.y = 0
six.rotation.x = 0

cinq.rotation.z = 0
cinq.rotation.y = 0
cinq.rotation.x = 0

cuquat.rotation.z = 0
cuquat.rotation.y = 0
cuquat.rotation.x = 0

cutrois.rotation.z = 0
cutrois.rotation.y = 0
cutrois.rotation.x = 0

cudeux.rotation.z = 0
cudeux.rotation.y = 0
cudeux.rotation.x = 0

fond.rotation.z = 0
fond.rotation.y = 0
fond.rotation.x = 0

 vincat.position.z = 1
vincat.position.y = -8
vincat.position.x = 18

vintrois.position.z = 0
vintrois.position.y = -8.3
vintrois.position.x = 19

deuxdeux.position.y = 0
deuxdeux.position.z = 0
deuxdeux.position.x = 17

vinun.position.z = 0
vinun.position.y = 5
vinun.position.x = 17

vingt.position.z = 0
vingt.position.y = 10
vingt.position.x = 17

dixneuf.position.z = -3
dixneuf.position.y = 5
dixneuf.position.x = 18

dixhuit.position.z = 0
dixhuit.position.y = -11
dixhuit.position.x = 3

dixsept.position.z = 0
dixsept.position.y = -11
dixsept.position.x = -1

seize.position.z = 0;
seize.position.y = -11;
seize.position.x = -5.5;

quinze.position.z = -1;
quinze.position.y = -3;
quinze.position.x = -1;

quatorze.position.z = -2
quatorze.position.y = -3
quatorze.position.x = -1

treize.position.z = -5
treize.position.y = -5
treize.position.x = -1

douze.position.z = -1
douze.position.y = 9
douze.position.x = 4

onze.position.z = -4.5
onze.position.y = 10
onze.position.x = 4

dix.position.z = -1
dix.position.y = 9
dix.position.x = -5

neuf.position.z = -4.5
neuf.position.y = 10
neuf.position.x = -5

huit.position.z = -1
huit.position.y = -8
huit.position.x = -19

sept.position.z = -4
sept.position.y = -9
sept.position.x = -21

six.position.z = -4
six.position.y = 0
six.position.x = -21

cinq.position.z = -10
cinq.position.y = -5
cinq.position.x = -25

cuquat.position.z = -4
cuquat.position.y = 11
cuquat.position.x = -18

cutrois.position.z = -4
cutrois.position.y = 11
cutrois.position.x = -25

cudeux.position.z = -10
cudeux.position.y = 12
cudeux.position.x = -25

fond.position.z = -20
fond.position.y = 0
fond.position.x = 0
}

function translateMode() {
  translate = !translate;
  rotate = false;
  chaos = false;
  console.log('Translate flipped');
}

function translateActivated() {
  fond.position.x += getRandomArbitrary(-5, 5);
fond.position.y += getRandomArbitrary(-5, 5);
fond.position.z += getRandomArbitrary(-5, 5);
cudeux.position.x += getRandomArbitrary(-5, 5);
cudeux.position.y += getRandomArbitrary(-5, 5);
cudeux.position.z += getRandomArbitrary(-5, 5);
cutrois.position.x += getRandomArbitrary(-5, 5);
cutrois.position.y += getRandomArbitrary(-5, 5);
cutrois.position.z += getRandomArbitrary(-5, 5);
cuquat.position.x += getRandomArbitrary(-5, 5);
cuquat.position.y += getRandomArbitrary(-5, 5);
cuquat.position.z += getRandomArbitrary(-5, 5);
cinq.position.x += getRandomArbitrary(-5, 5);
cinq.position.y += getRandomArbitrary(-5, 5);
cinq.position.z += getRandomArbitrary(-5, 5);
six.position.x += getRandomArbitrary(-5, 5);
six.position.y += getRandomArbitrary(-5, 5);
six.position.z += getRandomArbitrary(-5, 5);
sept.position.x += getRandomArbitrary(-5, 5);
sept.position.y += getRandomArbitrary(-5, 5);
sept.position.z += getRandomArbitrary(-5, 5);
huit.position.x += getRandomArbitrary(-5, 5);
huit.position.y += getRandomArbitrary(-5, 5);
huit.position.z += getRandomArbitrary(-5, 5);
neuf.position.x += getRandomArbitrary(-5, 5);
neuf.position.y += getRandomArbitrary(-5, 5);
neuf.position.z += getRandomArbitrary(-5, 5);
dix.position.x += getRandomArbitrary(-5, 5);
dix.position.y += getRandomArbitrary(-5, 5);
dix.position.z += getRandomArbitrary(-5, 5);
onze.position.x += getRandomArbitrary(-5, 5);
onze.position.y += getRandomArbitrary(-5, 5);
onze.position.z += getRandomArbitrary(-5, 5);
douze.position.x += getRandomArbitrary(-5, 5);
douze.position.y += getRandomArbitrary(-5, 5);
douze.position.z += getRandomArbitrary(-5, 5);
treize.position.x += getRandomArbitrary(-5, 5);
treize.position.y += getRandomArbitrary(-5, 5);
treize.position.z += getRandomArbitrary(-5, 5);
quatorze.position.x += getRandomArbitrary(-5, 5);
quatorze.position.y += getRandomArbitrary(-5, 5);
quatorze.position.z += getRandomArbitrary(-5, 5);
quinze.position.x += getRandomArbitrary(-5, 5);
quinze.position.y += getRandomArbitrary(-5, 5);
quinze.position.z += getRandomArbitrary(-5, 5);
seize.position.x += getRandomArbitrary(-5, 5);
seize.position.y += getRandomArbitrary(-5, 5);
seize.position.z += getRandomArbitrary(-5, 5);
dixsept.position.x += getRandomArbitrary(-5, 5);
dixsept.position.y += getRandomArbitrary(-5, 5);
dixsept.position.z += getRandomArbitrary(-5, 5);
dixhuit.position.x += getRandomArbitrary(-5, 5);
dixhuit.position.y += getRandomArbitrary(-5, 5);
dixhuit.position.z += getRandomArbitrary(-5, 5);
dixneuf.position.x += getRandomArbitrary(-5, 5);
dixneuf.position.y += getRandomArbitrary(-5, 5);
dixneuf.position.z += getRandomArbitrary(-5, 5);
vingt.position.x += getRandomArbitrary(-5, 5);
vingt.position.y += getRandomArbitrary(-5, 5);
vingt.position.z += getRandomArbitrary(-5, 5);
vinun.position.x += getRandomArbitrary(-5, 5);
vinun.position.y += getRandomArbitrary(-5, 5);
vinun.position.z += getRandomArbitrary(-5, 5);
deuxdeux.position.x += getRandomArbitrary(-5, 5);
deuxdeux.position.y += getRandomArbitrary(-5, 5);
deuxdeux.position.z += getRandomArbitrary(-5, 5);
vintrois.position.x += getRandomArbitrary(-5, 5);
vintrois.position.y += getRandomArbitrary(-5, 5);
vintrois.position.z += getRandomArbitrary(-5, 5);
vincat.position.x += getRandomArbitrary(-5, 5);
vincat.position.y += getRandomArbitrary(-5, 5);
vincat.position.z += getRandomArbitrary(-5, 5);
}

function rotateMode() {
  rotate = !rotate;
  translate = false;
  chaos = false;
  console.log('Rotate flipped');
}

function rotateActivated() {
  fond.rotation.x += getRandomArbitrary(-0.1, 0.1);
fond.rotation.y += getRandomArbitrary(-0.1, 0.1);
fond.rotation.z += getRandomArbitrary(-0.1, 0.1);
cudeux.rotation.x += getRandomArbitrary(-0.1, 0.1);
cudeux.rotation.y += getRandomArbitrary(-0.1, 0.1);
cudeux.rotation.z += getRandomArbitrary(-0.1, 0.1);
cutrois.rotation.x += getRandomArbitrary(-0.1, 0.1);
cutrois.rotation.y += getRandomArbitrary(-0.1, 0.1);
cutrois.rotation.z += getRandomArbitrary(-0.1, 0.1);
cuquat.rotation.x += getRandomArbitrary(-0.1, 0.1);
cuquat.rotation.y += getRandomArbitrary(-0.1, 0.1);
cuquat.rotation.z += getRandomArbitrary(-0.1, 0.1);
cinq.rotation.x += getRandomArbitrary(-0.1, 0.1);
cinq.rotation.y += getRandomArbitrary(-0.1, 0.1);
cinq.rotation.z += getRandomArbitrary(-0.1, 0.1);
six.rotation.x += getRandomArbitrary(-0.1, 0.1);
six.rotation.y += getRandomArbitrary(-0.1, 0.1);
six.rotation.z += getRandomArbitrary(-0.1, 0.1);
sept.rotation.x += getRandomArbitrary(-0.1, 0.1);
sept.rotation.y += getRandomArbitrary(-0.1, 0.1);
sept.rotation.z += getRandomArbitrary(-0.1, 0.1);
huit.rotation.x += getRandomArbitrary(-0.1, 0.1);
huit.rotation.y += getRandomArbitrary(-0.1, 0.1);
huit.rotation.z += getRandomArbitrary(-0.1, 0.1);
neuf.rotation.x += getRandomArbitrary(-0.1, 0.1);
neuf.rotation.y += getRandomArbitrary(-0.1, 0.1);
neuf.rotation.z += getRandomArbitrary(-0.1, 0.1);
dix.rotation.x += getRandomArbitrary(-0.1, 0.1);
dix.rotation.y += getRandomArbitrary(-0.1, 0.1);
dix.rotation.z += getRandomArbitrary(-0.1, 0.1);
onze.rotation.x += getRandomArbitrary(-0.1, 0.1);
onze.rotation.y += getRandomArbitrary(-0.1, 0.1);
onze.rotation.z += getRandomArbitrary(-0.1, 0.1);
douze.rotation.x += getRandomArbitrary(-0.1, 0.1);
douze.rotation.y += getRandomArbitrary(-0.1, 0.1);
douze.rotation.z += getRandomArbitrary(-0.1, 0.1);
treize.rotation.x += getRandomArbitrary(-0.1, 0.1);
treize.rotation.y += getRandomArbitrary(-0.1, 0.1);
treize.rotation.z += getRandomArbitrary(-0.1, 0.1);
quatorze.rotation.x += getRandomArbitrary(-0.1, 0.1);
quatorze.rotation.y += getRandomArbitrary(-0.1, 0.1);
quatorze.rotation.z += getRandomArbitrary(-0.1, 0.1);
quinze.rotation.x += getRandomArbitrary(-0.1, 0.1);
quinze.rotation.y += getRandomArbitrary(-0.1, 0.1);
quinze.rotation.z += getRandomArbitrary(-0.1, 0.1);
seize.rotation.x += getRandomArbitrary(-0.1, 0.1);
seize.rotation.y += getRandomArbitrary(-0.1, 0.1);
seize.rotation.z += getRandomArbitrary(-0.1, 0.1);
dixsept.rotation.x += getRandomArbitrary(-0.1, 0.1);
dixsept.rotation.y += getRandomArbitrary(-0.1, 0.1);
dixsept.rotation.z += getRandomArbitrary(-0.1, 0.1);
dixhuit.rotation.x += getRandomArbitrary(-0.1, 0.1);
dixhuit.rotation.y += getRandomArbitrary(-0.1, 0.1);
dixhuit.rotation.z += getRandomArbitrary(-0.1, 0.1);
dixneuf.rotation.x += getRandomArbitrary(-0.1, 0.1);
dixneuf.rotation.y += getRandomArbitrary(-0.1, 0.1);
dixneuf.rotation.z += getRandomArbitrary(-0.1, 0.1);
vingt.rotation.x += getRandomArbitrary(-0.1, 0.1);
vingt.rotation.y += getRandomArbitrary(-0.1, 0.1);
vingt.rotation.z += getRandomArbitrary(-0.1, 0.1);
vinun.rotation.x += getRandomArbitrary(-0.1, 0.1);
vinun.rotation.y += getRandomArbitrary(-0.1, 0.1);
vinun.rotation.z += getRandomArbitrary(-0.1, 0.1);
deuxdeux.rotation.x += getRandomArbitrary(-0.1, 0.1);
deuxdeux.rotation.y += getRandomArbitrary(-0.1, 0.1);
deuxdeux.rotation.z += getRandomArbitrary(-0.1, 0.1);
vintrois.rotation.x += getRandomArbitrary(-0.1, 0.1);
vintrois.rotation.y += getRandomArbitrary(-0.1, 0.1);
vintrois.rotation.z += getRandomArbitrary(-0.1, 0.1);
vincat.rotation.x += getRandomArbitrary(-0.1, 0.1);
vincat.rotation.y += getRandomArbitrary(-0.1, 0.1);
vincat.rotation.z += getRandomArbitrary(-0.1, 0.1);
}

function chaosMode() {
  chaos = !chaos
  translate = false;
  rotate = false;
  console.log('YOU WERE TOLD NOT TO PRESS YOU POOR SOUL');
}

function chaosActivated(){
  fond.position.x += getRandomArbitrary(-5, 5);
fond.position.y += getRandomArbitrary(-5, 5);
fond.position.z += getRandomArbitrary(-5, 5);
cudeux.position.x += getRandomArbitrary(-5, 5);
cudeux.position.y += getRandomArbitrary(-5, 5);
cudeux.position.z += getRandomArbitrary(-5, 5);
cutrois.position.x += getRandomArbitrary(-5, 5);
cutrois.position.y += getRandomArbitrary(-5, 5);
cutrois.position.z += getRandomArbitrary(-5, 5);
cuquat.position.x += getRandomArbitrary(-5, 5);
cuquat.position.y += getRandomArbitrary(-5, 5);
cuquat.position.z += getRandomArbitrary(-5, 5);
cinq.position.x += getRandomArbitrary(-5, 5);
cinq.position.y += getRandomArbitrary(-5, 5);
cinq.position.z += getRandomArbitrary(-5, 5);
six.position.x += getRandomArbitrary(-5, 5);
six.position.y += getRandomArbitrary(-5, 5);
six.position.z += getRandomArbitrary(-5, 5);
sept.position.x += getRandomArbitrary(-5, 5);
sept.position.y += getRandomArbitrary(-5, 5);
sept.position.z += getRandomArbitrary(-5, 5);
huit.position.x += getRandomArbitrary(-5, 5);
huit.position.y += getRandomArbitrary(-5, 5);
huit.position.z += getRandomArbitrary(-5, 5);
neuf.position.x += getRandomArbitrary(-5, 5);
neuf.position.y += getRandomArbitrary(-5, 5);
neuf.position.z += getRandomArbitrary(-5, 5);
dix.position.x += getRandomArbitrary(-5, 5);
dix.position.y += getRandomArbitrary(-5, 5);
dix.position.z += getRandomArbitrary(-5, 5);
onze.position.x += getRandomArbitrary(-5, 5);
onze.position.y += getRandomArbitrary(-5, 5);
onze.position.z += getRandomArbitrary(-5, 5);
douze.position.x += getRandomArbitrary(-5, 5);
douze.position.y += getRandomArbitrary(-5, 5);
douze.position.z += getRandomArbitrary(-5, 5);
treize.position.x += getRandomArbitrary(-5, 5);
treize.position.y += getRandomArbitrary(-5, 5);
treize.position.z += getRandomArbitrary(-5, 5);
quatorze.position.x += getRandomArbitrary(-5, 5);
quatorze.position.y += getRandomArbitrary(-5, 5);
quatorze.position.z += getRandomArbitrary(-5, 5);
quinze.position.x += getRandomArbitrary(-5, 5);
quinze.position.y += getRandomArbitrary(-5, 5);
quinze.position.z += getRandomArbitrary(-5, 5);
seize.position.x += getRandomArbitrary(-5, 5);
seize.position.y += getRandomArbitrary(-5, 5);
seize.position.z += getRandomArbitrary(-5, 5);
dixsept.position.x += getRandomArbitrary(-5, 5);
dixsept.position.y += getRandomArbitrary(-5, 5);
dixsept.position.z += getRandomArbitrary(-5, 5);
dixhuit.position.x += getRandomArbitrary(-5, 5);
dixhuit.position.y += getRandomArbitrary(-5, 5);
dixhuit.position.z += getRandomArbitrary(-5, 5);
dixneuf.position.x += getRandomArbitrary(-5, 5);
dixneuf.position.y += getRandomArbitrary(-5, 5);
dixneuf.position.z += getRandomArbitrary(-5, 5);
vingt.position.x += getRandomArbitrary(-5, 5);
vingt.position.y += getRandomArbitrary(-5, 5);
vingt.position.z += getRandomArbitrary(-5, 5);
vinun.position.x += getRandomArbitrary(-5, 5);
vinun.position.y += getRandomArbitrary(-5, 5);
vinun.position.z += getRandomArbitrary(-5, 5);
deuxdeux.position.x += getRandomArbitrary(-5, 5);
deuxdeux.position.y += getRandomArbitrary(-5, 5);
deuxdeux.position.z += getRandomArbitrary(-5, 5);
vintrois.position.x += getRandomArbitrary(-5, 5);
vintrois.position.y += getRandomArbitrary(-5, 5);
vintrois.position.z += getRandomArbitrary(-5, 5);
vincat.position.x += getRandomArbitrary(-5, 5);
vincat.position.y += getRandomArbitrary(-5, 5);
vincat.position.z += getRandomArbitrary(-5, 5);
      fond.rotation.x += getRandomArbitrary(-0.1, 0.1);
fond.rotation.y += getRandomArbitrary(-0.1, 0.1);
fond.rotation.z += getRandomArbitrary(-0.1, 0.1);
cudeux.rotation.x += getRandomArbitrary(-0.1, 0.1);
cudeux.rotation.y += getRandomArbitrary(-0.1, 0.1);
cudeux.rotation.z += getRandomArbitrary(-0.1, 0.1);
cutrois.rotation.x += getRandomArbitrary(-0.1, 0.1);
cutrois.rotation.y += getRandomArbitrary(-0.1, 0.1);
cutrois.rotation.z += getRandomArbitrary(-0.1, 0.1);
cuquat.rotation.x += getRandomArbitrary(-0.1, 0.1);
cuquat.rotation.y += getRandomArbitrary(-0.1, 0.1);
cuquat.rotation.z += getRandomArbitrary(-0.1, 0.1);
cinq.rotation.x += getRandomArbitrary(-0.1, 0.1);
cinq.rotation.y += getRandomArbitrary(-0.1, 0.1);
cinq.rotation.z += getRandomArbitrary(-0.1, 0.1);
six.rotation.x += getRandomArbitrary(-0.1, 0.1);
six.rotation.y += getRandomArbitrary(-0.1, 0.1);
six.rotation.z += getRandomArbitrary(-0.1, 0.1);
sept.rotation.x += getRandomArbitrary(-0.1, 0.1);
sept.rotation.y += getRandomArbitrary(-0.1, 0.1);
sept.rotation.z += getRandomArbitrary(-0.1, 0.1);
huit.rotation.x += getRandomArbitrary(-0.1, 0.1);
huit.rotation.y += getRandomArbitrary(-0.1, 0.1);
huit.rotation.z += getRandomArbitrary(-0.1, 0.1);
neuf.rotation.x += getRandomArbitrary(-0.1, 0.1);
neuf.rotation.y += getRandomArbitrary(-0.1, 0.1);
neuf.rotation.z += getRandomArbitrary(-0.1, 0.1);
dix.rotation.x += getRandomArbitrary(-0.1, 0.1);
dix.rotation.y += getRandomArbitrary(-0.1, 0.1);
dix.rotation.z += getRandomArbitrary(-0.1, 0.1);
onze.rotation.x += getRandomArbitrary(-0.1, 0.1);
onze.rotation.y += getRandomArbitrary(-0.1, 0.1);
onze.rotation.z += getRandomArbitrary(-0.1, 0.1);
douze.rotation.x += getRandomArbitrary(-0.1, 0.1);
douze.rotation.y += getRandomArbitrary(-0.1, 0.1);
douze.rotation.z += getRandomArbitrary(-0.1, 0.1);
treize.rotation.x += getRandomArbitrary(-0.1, 0.1);
treize.rotation.y += getRandomArbitrary(-0.1, 0.1);
treize.rotation.z += getRandomArbitrary(-0.1, 0.1);
quatorze.rotation.x += getRandomArbitrary(-0.1, 0.1);
quatorze.rotation.y += getRandomArbitrary(-0.1, 0.1);
quatorze.rotation.z += getRandomArbitrary(-0.1, 0.1);
quinze.rotation.x += getRandomArbitrary(-0.1, 0.1);
quinze.rotation.y += getRandomArbitrary(-0.1, 0.1);
quinze.rotation.z += getRandomArbitrary(-0.1, 0.1);
seize.rotation.x += getRandomArbitrary(-0.1, 0.1);
seize.rotation.y += getRandomArbitrary(-0.1, 0.1);
seize.rotation.z += getRandomArbitrary(-0.1, 0.1);
dixsept.rotation.x += getRandomArbitrary(-0.1, 0.1);
dixsept.rotation.y += getRandomArbitrary(-0.1, 0.1);
dixsept.rotation.z += getRandomArbitrary(-0.1, 0.1);
dixhuit.rotation.x += getRandomArbitrary(-0.1, 0.1);
dixhuit.rotation.y += getRandomArbitrary(-0.1, 0.1);
dixhuit.rotation.z += getRandomArbitrary(-0.1, 0.1);
dixneuf.rotation.x += getRandomArbitrary(-0.1, 0.1);
dixneuf.rotation.y += getRandomArbitrary(-0.1, 0.1);
dixneuf.rotation.z += getRandomArbitrary(-0.1, 0.1);
vingt.rotation.x += getRandomArbitrary(-0.1, 0.1);
vingt.rotation.y += getRandomArbitrary(-0.1, 0.1);
vingt.rotation.z += getRandomArbitrary(-0.1, 0.1);
vinun.rotation.x += getRandomArbitrary(-0.1, 0.1);
vinun.rotation.y += getRandomArbitrary(-0.1, 0.1);
vinun.rotation.z += getRandomArbitrary(-0.1, 0.1);
deuxdeux.rotation.x += getRandomArbitrary(-0.1, 0.1);
deuxdeux.rotation.y += getRandomArbitrary(-0.1, 0.1);
deuxdeux.rotation.z += getRandomArbitrary(-0.1, 0.1);
vintrois.rotation.x += getRandomArbitrary(-0.1, 0.1);
vintrois.rotation.y += getRandomArbitrary(-0.1, 0.1);
vintrois.rotation.z += getRandomArbitrary(-0.1, 0.1);
vincat.rotation.x += getRandomArbitrary(-0.1, 0.1);
vincat.rotation.y += getRandomArbitrary(-0.1, 0.1);
vincat.rotation.z += getRandomArbitrary(-0.1, 0.1);
}

function animate() {
    requestAnimationFrame( animate );
    if(chaos)
      chaosActivated();
    if (rotate)
      rotateActivated();
    if(translate)
      translateActivated();
    renderer.render( scene, camera );
}
animate();